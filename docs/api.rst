.. _introduction:

Pluggable Views API
===================

.. module:: fifty_flask.views.generic

Mixins
^^^^^^

.. autoclass:: ResponseMixin
   :members:
   :inherited-members:

.. autoclass:: ContextMixin
   :members:
   :inherited-members:

.. autoclass:: TemplateResponseMixin
   :members:
   :inherited-members:

.. autoclass:: TemplateMixin
   :members:

.. autoclass:: RedirectMixin
   :members:
   :inherited-members:

.. autoclass:: FormMixin
   :members:
   :inherited-members:

.. autoclass:: JsonResponseMixin
   :members:
   :inherited-members:

.. autoclass:: JsonMixin
   :members:
   :inherited-members:

.. autoclass:: MimeTypeResponseMixin
   :members:
   :inherited-members:

Views
^^^^^

.. autoclass:: GenericView
   :members:
   :inherited-members:

.. autoclass:: TemplateView
   :members:
   :inherited-members:

.. autoclass:: RedirectView
   :members:
   :inherited-members:

.. autoclass:: ProcessFormView
   :members:
   :inherited-members:

.. autoclass:: FormView
   :members:
   :inherited-members:

.. autoclass:: AjaxView
   :members:
   :inherited-members:

.. autoclass:: AjaxFormView
   :members:
   :inherited-members:
