import time

DEBUG = False
SECRET_KEY = 'zyxwvut'
TESTING = True
TZ = time.tzname
ENV_PREFIX = 'FIFTY'
